package com.demo.springboot.domain.dto;

public class DocumentDto {

    public DocumentDto(Integer a, Integer b, Integer c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    private Integer a;
    private Integer b;
    private Integer c;

    public DocumentDto(){

    }

//    public DocumentDto(Integer a, Integer b, Integer c){
//        this.a = a;
//        this.b = b;
//
//    }
    public Integer getA() {
        return a;
    }

    public Integer getB() {
        return b;
    }

    public Integer getC() {
        return c;
    }
}
